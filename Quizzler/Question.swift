//
//  Question.swift
//  Quizzler
//
//  Created by Matt Cheetham on 09/01/2019.
//  Copyright © 2019 London App Brewery. All rights reserved.
//

import Foundation

class Question {
    
    //properties of class
    //let is constant
    //var is variable
    let questionText : String
    let answer : Bool
    
    //initialisation method of event
    init(text: String, correctAnswer: Bool) {
        
        questionText = text
        answer = correctAnswer
    }
    
    //method is within a class
    func DoSomething() {
        
    }
}

//function is out of class scope
func DoSomethingElse() {
    
}
